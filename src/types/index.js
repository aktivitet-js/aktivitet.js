import activity from './activity.js';
import parent from './parent.js';
import sequential from './sequential.js';

const types = {
    activity,
    parent,
    sequential
};

export default types;
