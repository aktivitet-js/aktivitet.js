import selectors from "../selectors.js";
import delegate from "delegate-it";
import Parent from './parent.js';

import globals from '../globals.js';

selectors.SEQUENTIAL = '[data-activity="sequential"]';

class Sequential extends Parent
{
    init()
    {
        // lock all direct children, except the first.
        this.query( 'direct-children' )
            .forEach( function ( $child, i ) {
                if ( i > 0 && ! $child.activity.is_completed )
                {
                    $child.activity.is_locked = true;
                    $child.activity.update_classes();
                }
            } );
    }

    unlock_next( $child )
    {
        var $direct_children, $next;

        if ( $child.activity.is_completed )
        {
            $direct_children = Array.from( this.query( 'direct-children' ) );
            $next = $direct_children[ $direct_children.indexOf( $child ) + 1 ];
            if ( $next )
                $next.activity.unlock();
        }
    }

    auto_complete( $child )
    {
        this.unlock_next( $child );
        Parent.prototype.auto_complete.call( this, $child );
    }
}

export default Sequential;
