import selectors from "./selectors.js";
import globals from "./globals.js";

selectors.BUTTON_CLICK_ELS = "a,button";
selectors.BUTTON_COMPLETE = "[data-complete]";
selectors.BUTTON_START = "[data-start]";
selectors.BUTTON_UNLOCK = "[data-unlock]";

const button_types = [ selectors.BUTTON_COMPLETE, selectors.BUTTON_START, selectors.BUTTON_UNLOCK ];

globals.$event_root.addEventListener( 'click', function ( e ) {
    if ( e.target.matches( selectors.ACTIVITY + ' ' + selectors.BUTTON_CLICK_ELS ) && e.target.matches( button_types.join() ) )
    {
        const $button = e.target;
        const $activity = $button.closest( selectors.ACTIVITY );

        if ( $button.matches( selectors.BUTTON_COMPLETE ) )
            $activity.activity.complete();
        else if ( $button.matches( selectors.BUTTON_START ) )
            $activity.activity.start();
        else if ( $button.matches( selectors.BUTTON_UNLOCK ) )
            $activity.activity.unlock();

        $button.setAttribute( 'disabled', '' );
    }
} ); 
