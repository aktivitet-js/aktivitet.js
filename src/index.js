import globals from './globals.js';

import events from "./events.js";
import selectors from "./selectors.js";

import types from './types/index.js';

import './buttons.js';

import init from './init.js';

export default {
    init,
    events,
    selectors,
    types,
    globals
};
