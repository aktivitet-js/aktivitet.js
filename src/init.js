import selectors from './selectors.js';
import events from './events.js';
import types from './types/index.js';
import _new from './new.js';
import globals from './globals.js';

import load from './load.js';

const $event_root = globals.$event_root

export default function ( selector = selectors.ACTIVITY, options = null ) {
    options = options || {};

    document.querySelectorAll( selector ).forEach( function ( $activity ) {
        var load_state = load( $activity, options );
        return _new( $activity, options, load_state )
    } );

    $event_root.dispatchEvent( events.init() );
};
