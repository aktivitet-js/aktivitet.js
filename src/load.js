import globals from './globals.js';

// default null loader
function loader( $activity, options )
{
    console.warn( 'default null loader called with:',
                  '\n    $activity:', $activity,
                  '\n    options:', options,
                  '\nconsider implementing your own, or finding a package that has :)' );
    return {};
}

globals.loader = loader;

export default function load( $activity, options ) {
    return globals.loader( $activity, options );
};
