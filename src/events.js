function base( ev, options = null )
{
    if ( options === null )
    {
        options = {
            bubbles: true,
            cancelable: true
        }
    }
    return new Event( ev, options );
}

function started()
{
    return base( 'started' );
}
function completed()
{
    return base( 'completed' );
}
function unlocked()
{
    return base( 'unlocked' );
}
function loaded()
{
    return base( 'loaded' );
}
function init()
{
    return base( 'init', {} );
}

const events = {
    base,
    init,
    loaded,
    started,
    completed,
    unlocked
};

export default events;