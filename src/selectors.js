// selector constants
const ACTIVITY = "[data-activity]";
const DIRECT_CHILD = ACTIVITY + ":not( :scope " + ACTIVITY + " " + ACTIVITY + " )";
const CHILDREN_NOT_COMPLETE = ACTIVITY + ':not( .activity-completed )';
const DIRECT_CHILDREN_NOT_COMPLETE = DIRECT_CHILD + ':not( .activity-completed )';
const NOT_SCOPE = ':not( :scope )';
const ACTIVITY_NOT_SCOPE = ACTIVITY + NOT_SCOPE;

const selectors = {
    ACTIVITY,
    DIRECT_CHILD,
    CHILDREN_NOT_COMPLETE,
    DIRECT_CHILDREN_NOT_COMPLETE,
    NOT_SCOPE,
    ACTIVITY_NOT_SCOPE
};

export default selectors;